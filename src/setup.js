const child_process = require('child_process');
const fetch = require('sync-fetch');
const fs = require('fs-extra');
const path = require('path');
const { unzip } = require('cross-unzip');
const { getGamePath } = require('steam-game-path');

(async () => {
  //Clear gamedir directory
  console.info(`[Setup] Clearing ./tmp/gamedir`);
  try { fs.rmSync(path.resolve('./tmp/gamedir'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./tmp/gamedir')); } catch(err) {}

  //Get steam game path
  console.info(`[Setup] Searching for game in Steam`);
  let gamePathObject = getGamePath(887570, true);
  let gamePathString = path.resolve(gamePathObject.game.path);

  //Copy steam game into gamedir
  console.info(`[Setup] Copying Steam game files into ./tmp/gamedir`);
  try{ fs.copySync(gamePathString, path.resolve('./tmp/gamedir')); } catch(err) {}

  //Download Goldberg Emulator
  console.info(`[Setup] Downloading Goldberg Emulator`);
  fs.writeFileSync(path.resolve('./tmp/gamedir/goldbergemu.zip'), fetch('https://gitlab.com/Mr_Goldberg/goldberg_emulator/-/jobs/2975828369/artifacts/download').buffer());

  //Unzip Goldberg Emulator
  console.info(`[Setup] Unzipping Goldberg Emulator`);
  try { fs.mkdirSync(path.resolve('./tmp/gamedir/goldbergemu')); } catch(err) {}
  await new Promise((resolve) => {
    unzip(path.resolve('./tmp/gamedir/goldbergemu.zip'), path.resolve('./tmp/gamedir/goldbergemu'), () => {
      resolve();
    });
  });

  //Copy Goldberg Emulator DLL
  console.info(`[Setup] Copying Goldberg Emulator DLL`);
  try{ fs.copyFileSync(path.resolve('./tmp/gamedir/goldbergemu/steam_api64.dll'), path.resolve('./tmp/gamedir/Nebulous_Data/Plugins/x86_64/steam_api64.dll')); } catch(err) {}
  try { fs.rmSync(path.resolve('./tmp/gamedir/goldbergemu'), { recursive: true, force: true }); } catch(err) {}

  //Download BepInEx
  console.info(`[Setup] Downloading BepInEx`);
  fs.writeFileSync(path.resolve('./tmp/gamedir/bepinex.zip'), fetch('https://github.com/BepInEx/BepInEx/releases/download/v5.4.19/BepInEx_x64_5.4.19.0.zip').buffer());

  //Unzip BepInEx
  console.info(`[Setup] Unzipping BepInEx`);
  await new Promise((resolve) => {
    unzip(path.resolve('./tmp/gamedir/bepinex.zip'), path.resolve('./tmp/gamedir'), () => {
      resolve();
    });
  });

  //Run Nebulous to create BepInEx folders
  console.info(`[Setup] First time BepInEx setup`);
  let gameProcess = child_process.spawn('Nebulous.exe', ['-batchmode'], {
    cwd: path.resolve('./tmp/gamedir')
  });
  while(!fs.existsSync(path.resolve('./tmp/gamedir/BepInEx/plugins'))) {
    await new Promise((resolve) => { setTimeout(resolve, 1000); });
  }
  gameProcess.kill('SIGKILL');

  //Download Runtime Unity Editor
  console.info(`[Setup] Downloading Runtime Unity Editor`);
  fs.writeFileSync(path.resolve('./tmp/gamedir/rue.zip'), fetch('https://github.com/ManlyMarco/RuntimeUnityEditor/releases/download/v3.0/RuntimeUnityEditor_BepInEx5_v3.0.zip').buffer());

  //Unzip Runtime Unity Editor
  console.info(`[Setup] Unzipping Runtime Unity Editor`);
  await new Promise((resolve) => {
    unzip(path.resolve('./tmp/gamedir/rue.zip'), path.resolve('./tmp/gamedir/'), () => {
      resolve();
    });
  });

})();
