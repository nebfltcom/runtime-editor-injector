# Runtime Editor Inject

NEBULOUS: Fleet Command BepinEx plugin for automatic setup of Runtime Unity Editor (https://github.com/ManlyMarco/RuntimeUnityEditor).

# Usage

## Prerequisite

2) Node.js v16.13.2 and NPM 8.1.2
    - https://nodejs.org/en/download/

*Note: Exact versions are not strictly required and older versions may work. These version numbers are provided for replicating known working setup.*

## Automatic Installation

1) Git clone or download this repository (https://gitlab.com/nebfltcom/unit-tester/-/archive/main/unit-tester-main.zip). Unzip if needed.
2) Open up a command prompt or powershell session and navigate to the folder that contains this file.
3) Run the command `npm install`.
4) Run the command `npm run setup`.

## Usage

1) Run the modified game found in `tmp\gamedir\Nebulous.exe`
